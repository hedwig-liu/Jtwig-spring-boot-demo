package com.jtwig.demo.configuration;

import org.jtwig.environment.EnvironmentConfigurationBuilder;
import org.jtwig.extension.Extension;
import org.jtwig.spring.JtwigViewResolver;
import org.jtwig.spring.boot.config.JtwigViewResolverConfigurer;
import org.jtwig.web.servlet.JtwigRenderer;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuhui
 * @description
 * @date 2017/9/25
 * @since 1.0
 */
@Configuration
public class JtwigConfig implements JtwigViewResolverConfigurer {

    @Override
    public void configure(JtwigViewResolver viewResolver) {

        viewResolver.setPrefix("classpath:/WEB-INF/templates/");
        viewResolver.setSuffix(".twig.html");

        viewResolver.setRenderer(new JtwigRenderer(EnvironmentConfigurationBuilder
                .configuration()
                .extensions().add(new MyExtension()).and()
                .build()));
    }

    private static class MyExtension implements Extension {
        @Override
        public void configure(EnvironmentConfigurationBuilder configurationBuilder) {
        }
    }
}