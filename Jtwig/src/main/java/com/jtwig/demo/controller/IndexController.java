package com.jtwig.demo.controller;

import com.jtwig.demo.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhui
 * @description
 * @date 2017/9/25
 * @since 1.0
 */

@Controller
public class IndexController {


    @RequestMapping("/")
    public String index(Model model) {

        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            userList.add(new User(i, "<td style='color:red;'>名字<td>"+i, 10 + i));
        }

        model.addAttribute("userList",userList);
        return "index/index";
    }
}
