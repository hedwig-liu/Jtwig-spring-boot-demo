package com.jtwig.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuhui
 * @description
 * @date 2017/9/25
 * @since 1.0
 */
@SpringBootApplication(scanBasePackages = {"com.jtwig"})
@EnableAutoConfiguration
public class JtwigStart {

    public static void main(String[] args) {
        SpringApplication.run(JtwigStart.class, args);
    }
}
